package br.com.lead.collector.controllers;

import br.com.lead.collector.models.Produto;
import br.com.lead.collector.services.ProdutoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/produtos")
public class ProdutoController {

    @Autowired
    private ProdutoService produtoService;

    @PostMapping
    public Produto registrarProduto(@RequestBody Produto produto){
        return produtoService.salvarProduto(produto);
    }

    @GetMapping
    public Iterable<Produto> listarProduto(){
        Iterable<Produto> produtos =  produtoService.buscarTodosProdutos();
        return produtos;
    }

    @GetMapping("/{id}")
    public Produto buscaPorID(@RequestParam (name = "id", required = true) int id){
        Produto produtoPorID = produtoService.buscaPorId(id);
        return produtoPorID;
    }
}
